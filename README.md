# TechChallenge

Title: On the Beach Mobile Developer Test

Objective: Create a mobile application which downloads the flights and hotel JSON files and displays the information appropriately.

Specifications:

- Developed in Kotlin and AndroidX with its last versions.

- Coroutines to manage multi-threading.

- GSON and Retrofit for web service.

- Room for database management.

- ViewModel and LiveData to view communication and changes.

Followed steps:

1) Parse data

First of all, copy the JSON files in the test directory to be able to read them from unit tests.

The JSON is received in the web service communication channel. All the implementation will be organized in this layer.

Having two types of responses (Flight and Hotel) means to create a POJO class for each of them.

GSON library is used for (de)serialization.

The firsts tests implemented are only based on reading the JSON files and checking that the responses are created correctly.

Secondly, define the REST API interfaces to connect the web service and client. For now, they are simple without any parameter.

The Event class is used to manage success and handle error responses.

As retrofit is used to create the APIs, this library is tested using the MockWebServer implementation.

Finally, the tests check the web service implementation for any type of response received.

2) Save data

It has been decided to save the flight list on the local database to improve efficiency consulting the main view of the application.

Although the hotels are not saved, they could easily be too.

The instrumentation tests implemented to create a clean database in memory and load the JSON into the database.

3) Manage data

The Repository has the responsibility of managing the downloaded data and pass to the View Model which will provide to the View.

In the case of Flight management, they are downloaded from the server and then saved to the database.

Once stored the LiveData component triggers an event returning the entities list.

As mentioned before, as the Hotels are not stored in the database, the repository has less complexity.

The tests use mockito to fake web services and DAOs.

4) View Model

The next step before working with UI is creating the ViewModels.

They have a dependency on the Repository and uses its data to parse for the view.

Also, provide feedback about the current process of the requests.

The tests need extra configuration for running the view model scope when executing coroutines.

5) Show Data

The last step, show the data reactively.

Flights are shown in a list ordered by departure date, price and arrival airport.

For each flight, is shown the duration of the trip and the departure and arrival date at local time.

Arbitrarily, one flight is assigned to have a hotel offer. As a result, it can be selected to read the information.

The hotel screen has a View Pager to load the images of the hotel and displays everything in the data.

The architecture of the Application is based on one single activity and fragments for each screen.

For that reason, there are single fragment tests and activity tests.

On one hand, the fragment tests are very similar to unit tests because they are mocking the view model connection.

On the other hand, the activity test is working with real dependencies where it could be a good opportunity to capture app screenshots.

6) Accomplishments

The result is an application being able to download flights from the network and show it to the user responsively.

The flights with a hotel offer will access to a window with its information.

If the user has no internet connection, the flights downloaded before will be still available to consult.

The project has a high code coverage regarding the layers tested in the unit tests. The interval goes from 85% to 100%.

Besides, there are instrumentation tests for every activity/fragment and the database.

7) Improvements

- Handle internet connection errors and reconnection to refresh data.
- Add more functionality to the flight list: pull to refresh, filter, sort.
- Manage view model status when executing actions. (For instance, refresh the list)
- Add code coverage to instrumentation tests.
- Fake web service layer.