package aleixbdm.com.techchallenge.repository.flight

import aleixbdm.com.techchallenge.*
import aleixbdm.com.techchallenge.database.flight.FlightDao
import aleixbdm.com.techchallenge.database.flight.FlightEntity
import aleixbdm.com.techchallenge.database.flight.toEntity
import aleixbdm.com.techchallenge.repository.RepositoryStatus
import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import aleixbdm.com.techchallenge.webservice.flight.FlightListResponse
import aleixbdm.com.techchallenge.webservice.flight.FlightWebService
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import shared.EventTestData
import shared.fromJson
import shared.liveDataRule

class FlightRepositoryTest {

    @get:Rule
    val liveDataRule = liveDataRule()

    private object Module : FlightRepositoryModule

    private val converter = WebServiceInjector.converter()

    private val webService: FlightWebService = mock()
    private val dao: FlightDao = mock()
    private val testable = Module.repository(webService, dao)

    /** Flight List LiveData **/

    @Test
    fun flightListLiveData() {
        val json = readJson("flightList.json")
        val response: FlightListResponse = converter.fromJson(json)
        val entity = response.flightList.map { it.toEntity() }
        val data = EventTestData.success(entity)

        flightListLiveDataTest(
            expected = entity.map { it.toModel() },
            data = data
        )
    }

    @Test
    fun `flightListLiveData when error`() {
        flightListLiveDataTest(
            expected = null,
            data = EventTestData.error()
        )
    }

    private fun flightListLiveDataTest(
        expected: List<FlightModel>?,
        data: EventTestData<List<FlightEntity>>
    ) {
        whenCoroutine(dao) { getFlightList() }.doAnswer {
            data.toLiveData()
        }

        val actual = testable.flightListLiveData.getValueForTest()
        assertEquals(expected, actual)

        verifyCoroutine(dao) { getFlightList() }
    }

    /** Get Flight List **/

    @Test
    fun getFlightList() {
        val json = readJson("flightList.json")
        val response: FlightListResponse = converter.fromJson(json)
        val data = EventTestData.success(response)

        getFlightListTest(
            data = data
        )
    }

    @Test
    fun `getFlightList when error`() {
        getFlightListTest(
            data = EventTestData.error()
        )
    }

    private fun getFlightListTest(data: EventTestData<FlightListResponse>) {
        whenCoroutine(webService) { getFlightList() }.doAnswer {
            data.event
        }

        val event = synchronizedCoroutine {
            testable.getFlightList()
        }

        verifyCoroutine(webService) { getFlightList() }

        data.event.evaluate(onSuccess = { value ->
            val flightList = value.flightList.map { it.toEntity() }
            verifyCoroutine(dao) { insertFlightList(flightList) }
            assertEquals(RepositoryStatus.Completed, event.tryGetValue())

        }, onError = {
            val exception = try {
                event.tryGetValue()
            } catch (exception: Exception) {
                exception
            }
            assertEquals(it, exception)
        })
    }
}