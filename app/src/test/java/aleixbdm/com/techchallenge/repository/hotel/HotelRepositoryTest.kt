package aleixbdm.com.techchallenge.repository.hotel

import aleixbdm.com.techchallenge.*
import aleixbdm.com.techchallenge.repository.RepositoryStatus
import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import aleixbdm.com.techchallenge.webservice.hotel.HotelResponse
import aleixbdm.com.techchallenge.webservice.hotel.HotelWebService
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import shared.EventTestData
import shared.fromJson
import shared.liveDataRule

class HotelRepositoryTest {

    @get:Rule
    val liveDataRule = liveDataRule()

    private object Module : HotelRepositoryModule

    private val converter = WebServiceInjector.converter()

    private val webService: HotelWebService = mock()
    private val testable = Module.repository(webService)

    /** Hotel LiveData & Get Hotel **/

    @Test
    fun hotelLiveData() {
        val json = readJson("hotel.json")
        val response: HotelResponse = converter.fromJson(json)
        val data = EventTestData.success(response)

        hotelLiveDataTest(
            expected = response.toModel(),
            data = data
        )
    }

    @Test
    fun `hotelLiveData when error`() {
        hotelLiveDataTest(
            expected = null,
            data = EventTestData.error()
        )
    }

    private fun hotelLiveDataTest(expected: HotelModel?, data: EventTestData<HotelResponse>) {
        whenCoroutine(webService) { getHotel() }.doAnswer {
            data.event
        }

        val event = synchronizedCoroutine {
            testable.getHotel()
        }

        val actual = testable.hotelLiveData.getValueForTest()
        assertEquals(expected, actual)

        verifyCoroutine(webService) { getHotel() }

        data.event.evaluate(onSuccess = {
            assertEquals(RepositoryStatus.Completed, event.tryGetValue())

        }, onError = {
            val exception = try {
                event.tryGetValue()
            } catch (exception: Exception) {
                exception
            }
            assertEquals(it, exception)
        })
    }
}