package aleixbdm.com.techchallenge

interface TestChecker<in Ex, in Status> {
    fun check(executions: List<Ex>, status: Status)
}