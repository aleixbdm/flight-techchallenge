package aleixbdm.com.techchallenge.webservice.flight

import aleixbdm.com.techchallenge.readJson
import aleixbdm.com.techchallenge.webservice.URL_FlightList
import aleixbdm.com.techchallenge.webservice.WebServiceTest
import aleixbdm.com.techchallenge.webservice.WebServiceTestData
import aleixbdm.com.techchallenge.webservice.WebServiceTestStatus
import org.junit.Test
import retrofit2.Retrofit
import shared.TestModule

class FlightWebServiceTest : WebServiceTest<FlightWebService>(Module) {

    private object Module : RetrofitFlightWebServiceModule,
        TestModule<FlightWebService, Retrofit> {
        override fun resolve(resolver: Retrofit) =
            webService(resolver)
    }

    /** Get Flight List **/

    @Test
    fun `get flight list when success`() {
        val json = readJson("flightList.json")
        val data = WebServiceTestData.success(json)
        getFlightListTest(data)
    }

    @Test
    fun `get flight list when error`() {
        val data = WebServiceTestData.error()
        getFlightListTest(data)
    }

    @Test
    fun `get flight list when no data`() {
        val data = WebServiceTestData.noData()
        getFlightListTest(data)
    }

    @Test
    fun `get flight list when invalid data`() {
        val data = WebServiceTestData.invalid()
        getFlightListTest(data)
    }

    @Test
    fun `get flight list when empty`() {
        val data = WebServiceTestData.empty()
        getFlightListTest(data)
    }

    private fun getFlightListTest(data: WebServiceTestData) {
        val method = "GET"
        val path = URL_FlightList
        val status = WebServiceTestStatus(path, method)
        test(
            data,
            { it.getFlightList() },
            status
        )
    }
}