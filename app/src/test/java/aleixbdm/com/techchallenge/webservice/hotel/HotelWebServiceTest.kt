package aleixbdm.com.techchallenge.webservice.hotel

import aleixbdm.com.techchallenge.readJson
import aleixbdm.com.techchallenge.webservice.URL_Hotel
import aleixbdm.com.techchallenge.webservice.WebServiceTest
import aleixbdm.com.techchallenge.webservice.WebServiceTestData
import aleixbdm.com.techchallenge.webservice.WebServiceTestStatus
import org.junit.Test
import retrofit2.Retrofit
import shared.TestModule

class HotelWebServiceTest : WebServiceTest<HotelWebService>(Module) {

    private object Module : RetrofitHotelWebServiceModule,
        TestModule<HotelWebService, Retrofit> {
        override fun resolve(resolver: Retrofit) =
            webService(resolver)
    }

    /** Get Hotel **/

    @Test
    fun `get hotel when success`() {
        val json = readJson("hotel.json")
        val data = WebServiceTestData.success(json)
        getHotelTest(data)
    }

    @Test
    fun `get hotel when error`() {
        val data = WebServiceTestData.error()
        getHotelTest(data)
    }

    @Test
    fun `get hotel when no data`() {
        val data = WebServiceTestData.noData()
        getHotelTest(data)
    }

    @Test
    fun `get hotel when invalid data`() {
        val data = WebServiceTestData.invalid()
        getHotelTest(data)
    }

    @Test
    fun `get hotel when empty`() {
        val data = WebServiceTestData.empty()
        getHotelTest(data)
    }

    private fun getHotelTest(data: WebServiceTestData) {
        val method = "GET"
        val path = URL_Hotel
        val status = WebServiceTestStatus(path, method)
        test(
            data,
            { it.getHotel() },
            status
        )
    }
}