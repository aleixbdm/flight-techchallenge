package aleixbdm.com.techchallenge.webservice

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import retrofit2.Retrofit

/** Tester **/

interface RetrofitTester {
    fun start(): Retrofit
    fun enqueueRequest(data: WebServiceTestData)
    fun takeRequest(): RecordedRequest
    fun stop()
}

/** Definition **/

private class RetrofitTesterDefinition(
    private val server: MockWebServer
) : RetrofitTester {

    override fun start() = {
        server.start()
        val baseUrl = server.url("/")
        WebServiceInjector.retrofit(baseUrl)
    }()

    override fun stop() =
        server.shutdown()

    override fun enqueueRequest(data: WebServiceTestData) = data.run {
        server.enqueue(
            MockResponse()
                .setResponseCode(responseCode)
                .apply {
                    json?.let {
                        setBody(json)
                    }
                }
        )
    }

    override fun takeRequest(): RecordedRequest =
        server.takeRequest()
}

/** Module **/

object RetrofitTesterModule {
    fun tester(server: MockWebServer): RetrofitTester {
        return RetrofitTesterDefinition(server)
    }
}