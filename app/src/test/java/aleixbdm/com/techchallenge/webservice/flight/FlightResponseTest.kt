package aleixbdm.com.techchallenge.webservice.flight

import aleixbdm.com.techchallenge.date
import aleixbdm.com.techchallenge.readJson
import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import org.junit.Assert.assertEquals
import org.junit.Test
import shared.fromJson

class FlightResponseTest {

    private val converter = WebServiceInjector.converter()

    /** Parse FlightResponse **/

    @Test
    fun `parse flight json`() {
        val json = readJson("flight.json")
        val actual: FlightResponse = converter.fromJson(json)
        val expected = FlightResponse(
            airline = "FastJet",
            departureDate = date("2016-10-20T10:00:00Z"),
            arrivalDate = date("2016-10-20T11:00:00Z"),
            price = 12300,
            departureAirport = "London Gatwick",
            arrivalAirport = "Barcelona"
        )

        assertEquals(expected, actual)
    }

    /** Parse FlightListResponse **/

    @Test
    fun `parse flight list json`() {
        val json = readJson("flightList.json")
        val actual: FlightListResponse = converter.fromJson(json)
        val expected = 18
        assertEquals(expected, actual.flightList.size)
    }
}