package aleixbdm.com.techchallenge.webservice

import aleixbdm.com.techchallenge.TestChecker
import okhttp3.MediaType
import okhttp3.ResponseBody
import retrofit2.Response
import shared.fromJson
import java.net.HttpURLConnection

/** Data **/

class WebServiceTestData private constructor(
    val responseCode: Int,
    val json: String? = null
) {
    companion object {
        fun success(json: String) = WebServiceTestData(
            HttpURLConnection.HTTP_OK,
            json
        )

        fun noData() = WebServiceTestData(
            HttpURLConnection.HTTP_OK,
            null
        )

        fun invalid() = success("invalid")

        fun empty() = success("{}")

        fun error() = WebServiceTestData(
            HttpURLConnection.HTTP_INTERNAL_ERROR
        )
    }

    inline fun <reified T> toEvent() = toResponse<T>().toEvent()

    inline fun <reified T> toResponse(): Response<T> {
        return when (responseCode) {
            in 200..299 -> {
                try {
                    val converter = WebServiceInjector.converter()
                    val response: T? = converter.fromJson<T>(json)
                    Response.success(responseCode, response)
                } catch (exception: Exception) {
                    val errorBody = ResponseBody.create(
                        MediaType.parse("application/json"),
                        exception.message ?: ""
                    )
                    Response.error<T>(HttpURLConnection.HTTP_INTERNAL_ERROR, errorBody)
                }
            }
            else -> {
                val errorBody = ResponseBody.create(
                    MediaType.parse("application/json"),
                    json ?: ""
                )
                Response.error(responseCode, errorBody)
            }
        }
    }
}

/** Status **/

data class WebServiceTestStatus(
    val path: String,
    val method: String,
    val requestJson: String? = null
)

/** Test Checker **/

typealias WebServiceTestChecker<Ex> = TestChecker<Ex, WebServiceTestStatus>