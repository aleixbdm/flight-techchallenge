package aleixbdm.com.techchallenge.webservice

import com.google.gson.JsonParser
import okhttp3.mockwebserver.RecordedRequest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue

/** Definition **/

private object RetrofitTestChecker : WebServiceTestChecker<RecordedRequest> {
    override fun check(executions: List<RecordedRequest>, status: WebServiceTestStatus) =
        status.run {
            assertEquals(1, executions.size)
            val execution = executions.first()
            assertTrue(execution.path.contains(path))
            assertEquals(method, execution.method)
            val body = execution.body.readUtf8()
            if (body.isNotEmpty()) {
                val parser = JsonParser()
                val expected = parser.parse(requestJson)
                val actual = parser.parse(body)
                assertEquals(expected, actual)
            }
        }
}

/** Factory **/

object RetrofitTestCheckerFactory {
    fun checker(): WebServiceTestChecker<RecordedRequest> {
        return RetrofitTestChecker
    }
}