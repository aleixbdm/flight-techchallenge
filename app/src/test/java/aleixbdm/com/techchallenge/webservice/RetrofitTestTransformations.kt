package aleixbdm.com.techchallenge.webservice

import aleixbdm.com.techchallenge.core.Event
import retrofit2.Response

fun <T> Response<T>.toEvent(): Event<T> {
    return body()?.let {
        Event.success(it)
    } ?: run {
        Event.error<T>(WebServiceError(errorBody()?.string()))
    }
}