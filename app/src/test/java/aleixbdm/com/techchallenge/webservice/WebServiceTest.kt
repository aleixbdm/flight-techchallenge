package aleixbdm.com.techchallenge.webservice

import aleixbdm.com.techchallenge.core.Event
import aleixbdm.com.techchallenge.synchronizedCoroutine
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import retrofit2.Retrofit
import shared.TestModule

abstract class WebServiceTest<Testable : Any>(private val module: TestModule<Testable, Retrofit>) {

    lateinit var testable: Testable
    val retrofitTester = {
        val mockWebServer = MockWebServer()
        RetrofitTesterModule.tester(mockWebServer)
    }()
    val retrofitChecker =
        RetrofitTestCheckerFactory.checker()

    @Before
    fun before() {
        val retrofit = retrofitTester.start()
        testable = module.resolve(retrofit)
    }

    @After
    fun after() {
        retrofitTester.stop()
    }

    inline fun <reified Response> test(
        data: WebServiceTestData,
        crossinline testBlock: suspend (Testable) -> Event<Response>,
        status: WebServiceTestStatus
    ) {
        retrofitTester.enqueueRequest(data)
        val expected: Event<Response> = data.toEvent()
        val actual = synchronizedCoroutine {
            testBlock.invoke(testable)
        }

        assertEquals(expected, actual)

        val execution = retrofitTester.takeRequest()
        retrofitChecker.check(
            listOf(execution),
            status
        )
    }
}