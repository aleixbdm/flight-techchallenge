package aleixbdm.com.techchallenge

import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import shared.FailTestException
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.*

fun <T : Any> T.readJson(path: String): String {
    val file = javaClass.classLoader?.getResourceAsStream("assets/$path")
        ?: throw FailTestException("No file found on $path")
    return String(file.readBytes())
}

fun date(text: String): Date =
    Date.from(Instant.from(DateTimeFormatter.ISO_INSTANT.parse(text)))

/** In seconds **/
const val TEST_TIMEOUT = 1000L

@Throws(TimeoutCancellationException::class)
fun <T : Any> synchronizedCoroutine(block: suspend () -> T): T {
    lateinit var actual: T
    runBlocking {
        withTimeout(TEST_TIMEOUT) {
            actual = block()
        }
    }
    return actual
}