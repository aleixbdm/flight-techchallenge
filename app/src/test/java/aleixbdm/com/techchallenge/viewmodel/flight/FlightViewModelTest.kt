package aleixbdm.com.techchallenge.viewmodel.flight

import aleixbdm.com.techchallenge.*
import aleixbdm.com.techchallenge.core.Event
import aleixbdm.com.techchallenge.database.flight.toEntity
import aleixbdm.com.techchallenge.repository.RepositoryStatus
import aleixbdm.com.techchallenge.repository.flight.FlightModel
import aleixbdm.com.techchallenge.repository.flight.FlightRepository
import aleixbdm.com.techchallenge.repository.flight.toModel
import aleixbdm.com.techchallenge.viewmodel.ViewModelStatus
import aleixbdm.com.techchallenge.viewmodel.ViewModelTest
import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import aleixbdm.com.techchallenge.webservice.flight.FlightListResponse
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert.assertEquals
import org.junit.Test
import shared.EventTestData
import shared.fromJson

class FlightViewModelTest : ViewModelTest() {

    private val converter = WebServiceInjector.converter()

    private val repository: FlightRepository = mock()
    private val testable =
        FlightViewModelModule.factory(repository).create(FlightViewModel::class.java)

    /** Flight List LiveData **/

    @Test
    fun flightListLiveData() {
        val json = readJson("flightList.json")
        val response: FlightListResponse = converter.fromJson(json)
        val model = response.flightList.map { it.toEntity() }.map { it.toModel() }
        val data = EventTestData.success(model)

        flightListLiveDataTest(
            expected = model.map { it.toViewData() },
            data = data
        )
    }

    @Test
    fun `flightListLiveData when error`() {
        flightListLiveDataTest(
            expected = null,
            data = EventTestData.error()
        )
    }

    private fun flightListLiveDataTest(
        expected: List<FlightViewData>?,
        data: EventTestData<List<FlightModel>>
    ) {
        whenever(repository.flightListLiveData).doAnswer {
            data.toLiveData()
        }

        val actual = testable.flightListLiveData.getValueForTest()
        assertEquals(expected, actual)

        verify(repository).flightListLiveData
    }

    /** Get Flight List **/

    @Test
    fun getFlightList() {
        val status = RepositoryStatus.Completed
        val data = EventTestData.success(status)

        getFlightListTest(
            data = data
        )
    }

    @Test
    fun `getFlightList when error`() {
        getFlightListTest(
            data = EventTestData.error()
        )
    }

    private fun getFlightListTest(data: EventTestData<RepositoryStatus>) {
        whenCoroutine(repository) { getFlightList() }.doAnswer {
            data.event
        }

        val actual = testable.getFlightListStatus.captureValues(2) {
            testable.getFlightList()
        }
        val capturedValues = actual.values

        verifyCoroutine(repository) { getFlightList() }

        data.event.evaluate(onSuccess = {
            val expected = listOf(
                Event.success(ViewModelStatus.Loading),
                Event.success(ViewModelStatus.Completed)
            )
            assertEquals(expected, capturedValues)

        }, onError = {
            val expected = listOf(
                Event.success(ViewModelStatus.Loading),
                Event.error(it)
            )
            assertEquals(expected, capturedValues)
        })
    }
}