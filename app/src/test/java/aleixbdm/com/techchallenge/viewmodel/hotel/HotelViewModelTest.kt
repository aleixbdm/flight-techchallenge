package aleixbdm.com.techchallenge.viewmodel.hotel

import aleixbdm.com.techchallenge.*
import aleixbdm.com.techchallenge.core.Event
import aleixbdm.com.techchallenge.repository.RepositoryStatus
import aleixbdm.com.techchallenge.repository.hotel.HotelModel
import aleixbdm.com.techchallenge.repository.hotel.HotelRepository
import aleixbdm.com.techchallenge.repository.hotel.toModel
import aleixbdm.com.techchallenge.viewmodel.ViewModelStatus
import aleixbdm.com.techchallenge.viewmodel.ViewModelTest
import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import aleixbdm.com.techchallenge.webservice.hotel.HotelResponse
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert.assertEquals
import org.junit.Test
import shared.EventTestData
import shared.fromJson

class HotelViewModelTest : ViewModelTest() {

    private val converter = WebServiceInjector.converter()

    private val repository: HotelRepository = mock()
    private val testable =
        HotelViewModelModule.factory(repository).create(HotelViewModel::class.java)

    /** Hotel LiveData **/

    @Test
    fun hotelLiveData() {
        val json = readJson("hotel.json")
        val response: HotelResponse? = converter.fromJson(json)
        val model = response?.toModel()
        val data = EventTestData.success(model)

        hotelLiveDataTest(
            expected = model?.toViewData(),
            data = data
        )
    }

    @Test
    fun `hotelLiveData when error`() {
        hotelLiveDataTest(
            expected = null,
            data = EventTestData.error()
        )
    }

    private fun hotelLiveDataTest(expected: HotelViewData?, data: EventTestData<HotelModel?>) {
        whenever(repository.hotelLiveData).doAnswer {
            data.toLiveData()
        }

        val actual = testable.hotelLiveData.getValueForTest()
        assertEquals(expected, actual)

        verify(repository).hotelLiveData
    }

    /** Get Hotel **/

    @Test
    fun getHotel() {
        val status = RepositoryStatus.Completed
        val data = EventTestData.success(status)

        getHotelTest(
            data = data
        )
    }

    @Test
    fun `getHotel when error`() {
        getHotelTest(
            data = EventTestData.error()
        )
    }

    private fun getHotelTest(data: EventTestData<RepositoryStatus>) {
        whenCoroutine(repository) { getHotel() }.doAnswer {
            data.event
        }

        val actual = testable.getHotelStatus.captureValues(2) {
            testable.getHotel()
        }
        val capturedValues = actual.values

        verifyCoroutine(repository) { getHotel() }

        data.event.evaluate(onSuccess = {
            val expected = listOf(
                Event.success(ViewModelStatus.Loading),
                Event.success(ViewModelStatus.Completed)
            )
            assertEquals(expected, capturedValues)

        }, onError = {
            val expected = listOf(
                Event.success(ViewModelStatus.Loading),
                Event.error(it)
            )
            assertEquals(expected, capturedValues)
        })
    }
}