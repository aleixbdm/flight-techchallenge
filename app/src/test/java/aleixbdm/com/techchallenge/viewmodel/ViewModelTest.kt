package aleixbdm.com.techchallenge.viewmodel

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExecutorCoroutineDispatcher
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import shared.liveDataRule

/** Aware of warnings for
 * @ExperimentalCoroutinesApi
 * @ObsoleteCoroutinesApi
 **/
abstract class ViewModelTest {

    @get:Rule
    val liveDataRule = liveDataRule()

    private val mainThreadSurrogate: ExecutorCoroutineDispatcher =
        newSingleThreadContext("Test thread")

    @Before
    fun before() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun after() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }
}