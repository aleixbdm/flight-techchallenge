package aleixbdm.com.techchallenge

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking

fun <T, R> whenCoroutine(mock: T, function: suspend T.() -> R) =
    runBlocking { whenever(mock.function()) }

fun <T> verifyCoroutine(mock: T, function: suspend T.() -> Unit) {
    val verification = verify(mock)
    runBlocking { verification.function() }
}