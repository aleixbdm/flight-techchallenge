package aleixbdm.com.techchallenge.app

import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Before

abstract class AndroidTest {

    abstract val component: AppComponent

    @Before
    fun before() {
        val instrumentation = InstrumentationRegistry.getInstrumentation()
        val app = instrumentation.targetContext.applicationContext as TestApp

        app.testComponent = component
    }
}