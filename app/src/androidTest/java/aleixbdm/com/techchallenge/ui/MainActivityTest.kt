package aleixbdm.com.techchallenge.ui

import aleixbdm.com.techchallenge.R
import aleixbdm.com.techchallenge.app.RealDependenciesTest
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest : RealDependenciesTest() {

    @get:Rule
    var activityRule = ActivityTestRule(
        MainActivity::class.java,
        true,
        false
    )

    @Test
    fun check_FlightRecyclerView_displayed() {
        activityRule.launchActivity(null)
        onView(withId(R.id.flightRecyclerView))
            .check(matches(isDisplayed()))

        /** Time for some screenshots */
    }
}