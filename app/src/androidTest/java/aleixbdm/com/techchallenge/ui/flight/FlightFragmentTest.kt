package aleixbdm.com.techchallenge.ui.flight

import aleixbdm.com.techchallenge.R
import aleixbdm.com.techchallenge.app.AndroidTest
import aleixbdm.com.techchallenge.app.ManualAppComponent
import aleixbdm.com.techchallenge.app.TestViewModelFactory
import aleixbdm.com.techchallenge.database.flight.toEntity
import aleixbdm.com.techchallenge.matchers.RecyclerViewMatchers.hasItemCount
import aleixbdm.com.techchallenge.readJson
import aleixbdm.com.techchallenge.repository.flight.toModel
import aleixbdm.com.techchallenge.testContext
import aleixbdm.com.techchallenge.viewmodel.ManualViewModelResolverModule
import aleixbdm.com.techchallenge.viewmodel.flight.FlightViewData
import aleixbdm.com.techchallenge.viewmodel.flight.FlightViewModel
import aleixbdm.com.techchallenge.viewmodel.flight.toViewData
import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import aleixbdm.com.techchallenge.webservice.flight.FlightListResponse
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import org.junit.runner.RunWith
import shared.EventTestData
import shared.fromJson

@RunWith(AndroidJUnit4::class)
class FlightFragmentTest : AndroidTest() {

    private val converter = WebServiceInjector.converter()
    private val viewModel: FlightViewModel = mock()
    override val component = ManualAppComponent.inject(
        ManualViewModelResolverModule.viewModelResolver(
            viewModelsMap = mapOf(
                FlightViewModel::class.java to lazy { TestViewModelFactory(viewModel) }
            )
        )
    )

    /** Flight List LiveData **/

    @Test
    fun flightListLiveData() {
        val context = testContext()
        val json = context.readJson("flightList.json")
        val response: FlightListResponse? = converter.fromJson(json)
        val viewData = response?.flightList
            ?.map { it.toEntity() }?.map { it.toModel() }?.map { it.toViewData() }?.distinct()
            ?: emptyList()
        val data = EventTestData.success(viewData)

        flightListLiveDataTest(
            data = data
        )
    }

    @Test
    fun flightListLiveData_when_error() {
        flightListLiveDataTest(
            data = EventTestData.error()
        )
    }

    private fun flightListLiveDataTest(data: EventTestData<List<FlightViewData>>) {
        whenever(viewModel.flightListLiveData).doAnswer {
            data.toLiveData()
        }

        launchFragmentInContainer<FlightFragment>()

        verify(viewModel).flightListLiveData

        data.event.evaluate(onSuccess = {
            onView(withId(R.id.flightRecyclerView)).check(matches(hasItemCount(it.size)))
        }, onError = {
            onView(withId(R.id.flightRecyclerView)).check(matches(hasItemCount(0)))
        })
    }
}