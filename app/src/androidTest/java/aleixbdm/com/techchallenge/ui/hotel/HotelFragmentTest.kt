package aleixbdm.com.techchallenge.ui.hotel

import aleixbdm.com.techchallenge.R
import aleixbdm.com.techchallenge.app.AndroidTest
import aleixbdm.com.techchallenge.app.ManualAppComponent
import aleixbdm.com.techchallenge.app.TestViewModelFactory
import aleixbdm.com.techchallenge.readJson
import aleixbdm.com.techchallenge.repository.hotel.toModel
import aleixbdm.com.techchallenge.testContext
import aleixbdm.com.techchallenge.ui.progress.PROGRESS_BAR_LAYOUT_TAG
import aleixbdm.com.techchallenge.viewmodel.ManualViewModelResolverModule
import aleixbdm.com.techchallenge.viewmodel.hotel.HotelViewData
import aleixbdm.com.techchallenge.viewmodel.hotel.HotelViewModel
import aleixbdm.com.techchallenge.viewmodel.hotel.toViewData
import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import aleixbdm.com.techchallenge.webservice.hotel.HotelResponse
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.nhaarman.mockitokotlin2.*
import org.hamcrest.Matchers.`is`
import org.junit.Test
import org.junit.runner.RunWith
import shared.EventTestData
import shared.fromJson

@RunWith(AndroidJUnit4::class)
class HotelFragmentTest : AndroidTest() {

    private val converter = WebServiceInjector.converter()
    private val viewModel: HotelViewModel = mock()
    override val component = ManualAppComponent.inject(
        ManualViewModelResolverModule.viewModelResolver(
            viewModelsMap = mapOf(
                HotelViewModel::class.java to lazy { TestViewModelFactory(viewModel) }
            )
        )
    )

    /** Hotel LiveData **/

    @Test
    fun hotelLiveData() {
        val context = testContext()
        val json = context.readJson("hotel.json")
        val response: HotelResponse? = converter.fromJson(json)
        val viewData = response?.toModel()?.toViewData()
        val data = EventTestData.success(viewData)

        hotelLiveDataTest(
            data = data
        )
    }

    @Test
    fun hotelLiveData_when_error() {
        hotelLiveDataTest(
            data = EventTestData.error()
        )
    }

    private fun hotelLiveDataTest(data: EventTestData<HotelViewData?>) {
        whenever(viewModel.hotelLiveData).doAnswer {
            data.toLiveData()
        }

        val args = HotelFragmentArgs("Test Arrival Airport").toBundle()
        launchFragmentInContainer<HotelFragment>(args)

        verify(viewModel, times(2)).hotelLiveData

        data.event.evaluate(onSuccess = {
            onView(withId(R.id.hotelNameTextView))
                .check(matches(withText(it?.name)))
            onView(withId(R.id.hotelLocationTextView))
                .check(matches(withText(it?.location)))
            onView(withId(R.id.hotelDescriptionTextView))
                .check(matches(withText(it?.description)))
            onView(withId(R.id.hotelRatingTextView))
                .check(
                    matches(
                        withText(
                            testContext().getString(R.string.hotel_rating_value, it?.rating)
                        )
                    )
                )
            onView(withId(R.id.hotelFacilitiesTextView))
                .check(
                    matches(
                        withText(
                            it?.facilities?.joinToString()
                        )
                    )
                )

            onView(withTagValue(`is`(PROGRESS_BAR_LAYOUT_TAG)))
                .check(doesNotExist())
        }, onError = {
            onView(withTagValue(`is`(PROGRESS_BAR_LAYOUT_TAG)))
                .check(matches(isDisplayed()))
        })
    }
}