package aleixbdm.com.techchallenge.database.flight

import aleixbdm.com.techchallenge.database.AppDatabase
import aleixbdm.com.techchallenge.database.DatabaseTest
import aleixbdm.com.techchallenge.readJson
import aleixbdm.com.techchallenge.testContext
import aleixbdm.com.techchallenge.waitForLiveDataValue
import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import aleixbdm.com.techchallenge.webservice.flight.FlightListResponse
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import shared.TestModule
import shared.fromJson

@RunWith(AndroidJUnit4::class)
class FlightDaoTest : DatabaseTest<FlightDao>(Module) {

    private val converter = WebServiceInjector.converter()

    private object Module : FlightDaoModule,
        TestModule<FlightDao, AppDatabase> {
        override fun resolve(resolver: AppDatabase) =
            dao(resolver)
    }

    /** Get & Insert Flight List **/

    @Test
    fun initial_status() {
        val actual = waitForLiveDataValue(
            testable.getFlightList()
        )
        assertEquals(0, actual.size)
    }

    @Test
    fun getFlightList_and_insertFlightList() {
        val context = testContext()
        val json = context.readJson("flightList.json")
        val response: FlightListResponse = converter.fromJson(json)
        val listEntity = response.flightList.map { it.toEntity() }
        testable.insertFlightList(listEntity)
        val expected = listEntity.distinct()
        val actual = waitForLiveDataValue(
            testable.getFlightList()
        )
        assertEquals(expected.size, actual.size)
        assertTrue(expected.containsAll(actual))
    }
}