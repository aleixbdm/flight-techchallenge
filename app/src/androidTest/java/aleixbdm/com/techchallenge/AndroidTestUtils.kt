package aleixbdm.com.techchallenge

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.test.core.app.ApplicationProvider
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

fun testContext(): Context =
    ApplicationProvider.getApplicationContext<Context>()

fun Context.readJson(path: String): String {
    val file = assets.open(path)
    return String(file.readBytes())
}

/** Wait for the LiveData value for 2 seconds and then stop observing **/
@Throws(InterruptedException::class)
fun <T> waitForLiveDataValue(liveData: LiveData<T>): T {
    val data = arrayOfNulls<Any>(1)
    val latch = CountDownLatch(1)
    liveData.observeForever { o ->
        data[0] = o
        latch.countDown()
    }
    latch.await(2, TimeUnit.SECONDS)

    @Suppress("UNCHECKED_CAST")
    return data[0] as T
}
