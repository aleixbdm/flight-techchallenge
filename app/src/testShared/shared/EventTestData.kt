package shared

import aleixbdm.com.techchallenge.core.Event
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/** Data **/

class EventTestData<T> private constructor(
    val event: Event<T>
) {
    companion object {
        fun <T> success(data: T) = EventTestData(
            Event.success(data)
        )

        fun <T> error() = EventTestData(
            Event.error<T>(FailTestException("Error to test"))
        )
    }

    fun toLiveData(): LiveData<T> {
        val liveData = MutableLiveData<T>()
        try {
            val value = event.tryGetValue()
            liveData.value = value
        } catch (ex: Exception) {
        }
        return liveData
    }
}