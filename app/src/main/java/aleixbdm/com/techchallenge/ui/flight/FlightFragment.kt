package aleixbdm.com.techchallenge.ui.flight

import aleixbdm.com.techchallenge.R
import aleixbdm.com.techchallenge.app.InjectableFragment
import aleixbdm.com.techchallenge.app.resolveViewModel
import aleixbdm.com.techchallenge.viewmodel.flight.FlightViewModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.flight_fragment.*

class FlightFragment : InjectableFragment() {

    private val flightViewModel: FlightViewModel by resolveViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.flight_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = FlightAdapter()

        flightViewModel.flightListLiveData.observe(this, Observer {
            adapter.submitList(it)
        })
        lifecycle.addObserver(flightViewModel)

        flightRecyclerView.apply {
            this.adapter = adapter
        }
    }
}