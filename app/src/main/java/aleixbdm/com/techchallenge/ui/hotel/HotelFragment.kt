package aleixbdm.com.techchallenge.ui.hotel

import aleixbdm.com.techchallenge.R
import aleixbdm.com.techchallenge.app.InjectableFragment
import aleixbdm.com.techchallenge.app.resolveViewModel
import aleixbdm.com.techchallenge.ui.progress.progressBarLayout
import aleixbdm.com.techchallenge.viewmodel.hotel.HotelViewData
import aleixbdm.com.techchallenge.viewmodel.hotel.HotelViewModel
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.hotel_fragment.*

class HotelFragment : InjectableFragment() {

    private val hotelViewModel: HotelViewModel by resolveViewModel()
    private lateinit var progressLayout: LinearLayout

    private val hotelObserver =
        Observer<HotelViewData?> { value ->
            value?.let {
                hotelNameTextView.text = it.name
                hotelLocationTextView.text = it.location
                hotelRatingTextView.text = getString(R.string.hotel_rating_value, it.rating)
                hotelImagesViewPager.adapter =
                    HotelImagePagerAdapter(childFragmentManager, it.images)
                hotelDescriptionTextView.text = it.description
                hotelFacilitiesTextView.text = it.facilities.joinToString()

                (view as? ViewGroup)?.removeView(progressLayout)
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.hotel_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val safeArgs: HotelFragmentArgs by navArgs()
        // Could use the information from the flight to search a convenient hotel
        Log.i(this.javaClass.name, "Passed 'arrivalAirport': ${safeArgs.arrivalAirport}")

        progressLayout = progressBarLayout(view)
        (view as? ViewGroup)?.addView(progressLayout, 0)

        hotelViewModel.hotelLiveData.value?.let {
            hotelObserver.onChanged(it)
        }
        hotelViewModel.hotelLiveData.observe(this, hotelObserver)
        lifecycle.addObserver(hotelViewModel)
    }
}