package aleixbdm.com.techchallenge.ui.hotel

import aleixbdm.com.techchallenge.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.hotel_image.*

/** View Pager **/

class HotelImagePagerAdapter(
    fm: FragmentManager,
    private val images: List<String>
) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount() = images.size

    override fun getItem(i: Int): Fragment {
        val fragment = HotelImageFragment()
        fragment.arguments = Bundle().apply {
            putString(ARG_HOTEL_IMAGE, images[i])
        }
        return fragment
    }
}

private const val ARG_HOTEL_IMAGE = "object"

/** Pager Fragment **/

class HotelImageFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.hotel_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arguments?.takeIf { it.containsKey(ARG_HOTEL_IMAGE) }?.apply {
            val image = getString(ARG_HOTEL_IMAGE)
            Glide.with(view.context).load(image).into(hotelImageView)
        }
    }
}