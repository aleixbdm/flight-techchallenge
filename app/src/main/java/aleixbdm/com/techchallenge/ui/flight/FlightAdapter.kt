package aleixbdm.com.techchallenge.ui.flight

import aleixbdm.com.techchallenge.R
import aleixbdm.com.techchallenge.viewmodel.flight.FlightViewData
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import java.text.DecimalFormat

class FlightAdapter :
    ListAdapter<FlightViewData, FlightAdapter.ViewHolder>(FlightDiffCallback()) {

    class ViewHolder(val item: View) : RecyclerView.ViewHolder(item)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val flightViewData = getItem(position)
        val context = holder.item.context

        holder.item.findViewById<ImageView>(R.id.flightItemAirlineImageView)
            .setImageResource(flightViewData.airLineImageResource)

        holder.item.findViewById<TextView>(R.id.flightItemAirlineNameTextView).text =
            flightViewData.airlineName

        holder.item.findViewById<TextView>(R.id.flightItemDepartureAirportTextView).text =
            flightViewData.departureAirport

        holder.item.findViewById<TextView>(R.id.flightItemDepartureTimeTextView).text =
            flightViewData.departureTime

        holder.item.findViewById<TextView>(R.id.flightItemArrivalAirportTextView).text =
            flightViewData.arrivalAirport

        holder.item.findViewById<TextView>(R.id.flightItemArrivalTimeTextView).text =
            flightViewData.arrivalTime

        holder.item.findViewById<TextView>(R.id.flightItemDurationTextView).text =
            flightViewData.duration

        holder.item.findViewById<TextView>(R.id.flightItemDepartureDateTextView).text =
            flightViewData.departureDate

        val price = flightViewData.price.toDouble() / 100
        val format = context.getString(R.string.flight_item_price_format)
        holder.item.findViewById<TextView>(R.id.flightItemPriceTextView).text =
            DecimalFormat(format).format(price)

        holder.item.findViewById<TextView>(R.id.flightItemHotelTextView).visibility =
            if (position == 2) {
                // Hardcoded as an example
                holder.item.isClickable = true
                holder.item.setOnClickListener {
                    val action = FlightFragmentDirections.actionFlightListToHotel(
                        arrivalAirport = flightViewData.arrivalAirport
                    )
                    holder.item.findNavController().navigate(action)
                }

                View.VISIBLE
            } else {
                holder.item.isClickable = false

                View.GONE
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.flight_view_item, parent, false)
        return ViewHolder(itemView)
    }
}

private class FlightDiffCallback : DiffUtil.ItemCallback<FlightViewData>() {

    override fun areItemsTheSame(oldItem: FlightViewData, newItem: FlightViewData): Boolean {
        return oldItem.airlineName == newItem.airlineName &&
                oldItem.departureDate == newItem.departureDate &&
                oldItem.price == newItem.price &&
                oldItem.arrivalAirport == newItem.arrivalAirport
    }

    override fun areContentsTheSame(oldItem: FlightViewData, newItem: FlightViewData): Boolean {
        return oldItem == newItem
    }
}