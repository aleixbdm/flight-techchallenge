package aleixbdm.com.techchallenge.ui.progress

import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar

const val PROGRESS_BAR_LAYOUT_TAG = "progressBarLayout"
private const val PROGRESS_BAR_WIDTH = 100
private const val PROGRESS_BAR_HEIGHT = 100

fun progressBarLayout(view: View): LinearLayout {
    val context = view.context
    val parent = LinearLayout(context)
    parent.layoutParams = LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.MATCH_PARENT,
        LinearLayout.LayoutParams.MATCH_PARENT
    )
    parent.gravity = Gravity.CENTER
    parent.tag = PROGRESS_BAR_LAYOUT_TAG

    val progressBar = ProgressBar(context)
    progressBar.layoutParams = LinearLayout.LayoutParams(PROGRESS_BAR_WIDTH, PROGRESS_BAR_HEIGHT)
    parent.addView(progressBar)
    return parent
}