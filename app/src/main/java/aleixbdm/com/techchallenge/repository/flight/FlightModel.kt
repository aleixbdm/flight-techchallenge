package aleixbdm.com.techchallenge.repository.flight

import aleixbdm.com.techchallenge.database.flight.FlightEntity
import java.time.ZoneId
import java.util.*

/** Model **/

data class FlightModel(
    val airline: AirLine,
    val departureDate: Date,
    val arrivalDate: Date,
    val price: Long,
    val departureAirport: Airport,
    val arrivalAirport: Airport
)

enum class AirLine(val value: String) {
    FastJet("FastJet"),
    QuickJet("QuickJet"),
    SlowJet("SlowJet");

    companion object {
        fun from(value: String) = values().first { it.value == value }
    }
}

enum class Airport(val value: String) {
    LGW("London Gatwick"),
    BCN("Barcelona"),
    GRO("Girona"),
    AYT("Antalya");

    companion object {
        fun from(value: String) = values().first { it.value == value }
    }

    fun zoneId(): ZoneId = {
        val value = when (this) {
            LGW -> "Europe/London"
            BCN -> "Europe/Madrid"
            GRO -> "Europe/Madrid"
            AYT -> "Europe/Istanbul"
        }
        ZoneId.of(value)
    }()
}

/** Conversion **/

fun FlightEntity.toModel() = FlightModel(
    airline = AirLine.from(airline),
    departureDate = Date(departureDate),
    arrivalDate = Date(arrivalDate),
    price = price,
    departureAirport = Airport.from(departureAirport),
    arrivalAirport = Airport.from(arrivalAirport)
)