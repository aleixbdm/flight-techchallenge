package aleixbdm.com.techchallenge.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations

fun <X, Y> kotlinTransformationsMap(source: LiveData<X>, mapFunction: (X?) -> Y): LiveData<Y?> =
    Transformations.map(source, mapFunction)

fun <X, Y> kotlinTransformationsMapList(
    source: LiveData<List<X>>,
    mapFunction: (List<X>) -> List<Y>
): LiveData<List<Y>> = {
    val mapListFunction = { list: List<X>? ->
        val nonNullList = list ?: emptyList()
        mapFunction(nonNullList)
    }
    Transformations.map(source, mapListFunction)
}()