package aleixbdm.com.techchallenge.repository.flight

import aleixbdm.com.techchallenge.core.Event
import aleixbdm.com.techchallenge.database.flight.FlightDao
import aleixbdm.com.techchallenge.database.flight.toEntity
import aleixbdm.com.techchallenge.repository.RepositoryStatus
import aleixbdm.com.techchallenge.repository.kotlinTransformationsMapList
import aleixbdm.com.techchallenge.webservice.flight.FlightWebService
import androidx.lifecycle.LiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/** Repository **/

interface FlightRepository {
    val flightListLiveData: LiveData<List<FlightModel>>
    suspend fun getFlightList(): Event<RepositoryStatus>
}

/** Definition **/

private class FlightRepositoryDefinition(
    private val webService: FlightWebService,
    private val dao: FlightDao
) : FlightRepository {

    override val flightListLiveData by lazy(LazyThreadSafetyMode.NONE) {
        val liveData = dao.getFlightList()
        kotlinTransformationsMapList(liveData) { list ->
            list.map { it.toModel() }
        }
    }

    override suspend fun getFlightList(): Event<RepositoryStatus> {
        val event = webService.getFlightList()
        return try {
            val response = event.tryGetValue()
            withContext(Dispatchers.IO) {
                val flightList = response.flightList.map { it.toEntity() }
                dao.insertFlightList(flightList)
            }
            Event.success(RepositoryStatus.Completed)
        } catch (exception: Exception) {
            Event.error(exception)
        }
    }
}

/** Module **/

interface FlightRepositoryModule {
    fun repository(webService: FlightWebService, dao: FlightDao): FlightRepository =
        FlightRepositoryDefinition(webService, dao)
}