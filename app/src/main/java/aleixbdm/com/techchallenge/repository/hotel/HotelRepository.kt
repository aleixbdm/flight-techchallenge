package aleixbdm.com.techchallenge.repository.hotel

import aleixbdm.com.techchallenge.core.Event
import aleixbdm.com.techchallenge.repository.RepositoryStatus
import aleixbdm.com.techchallenge.repository.kotlinTransformationsMap
import aleixbdm.com.techchallenge.webservice.hotel.HotelResponse
import aleixbdm.com.techchallenge.webservice.hotel.HotelWebService
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/** Repository **/

interface HotelRepository {
    val hotelLiveData: LiveData<HotelModel?>
    suspend fun getHotel(): Event<RepositoryStatus>
}

/** Definition **/

private class HotelRepositoryDefinition(
    private val webService: HotelWebService
) : HotelRepository {

    private val _hotelLiveData = MutableLiveData<HotelResponse>()
    override val hotelLiveData by lazy(LazyThreadSafetyMode.NONE) {
        kotlinTransformationsMap(_hotelLiveData) { it?.toModel() }
    }

    override suspend fun getHotel(): Event<RepositoryStatus> {
        val event = webService.getHotel()
        return try {
            val response = event.tryGetValue()
            _hotelLiveData.postValue(response)
            Event.success(RepositoryStatus.Completed)
        } catch (exception: Exception) {
            Event.error(exception)
        }
    }
}

/** Module **/

interface HotelRepositoryModule {
    fun repository(webService: HotelWebService): HotelRepository =
        HotelRepositoryDefinition(webService)
}