package aleixbdm.com.techchallenge.repository.hotel

import aleixbdm.com.techchallenge.webservice.hotel.HotelResponse

/** Model **/

typealias HotelModel = HotelResponse

/** Conversion **/

fun HotelResponse.toModel() = HotelModel(
    name = name,
    location = location,
    description = description,
    images = images,
    rating = rating,
    facilities = facilities
)