package aleixbdm.com.techchallenge.webservice.hotel

import aleixbdm.com.techchallenge.webservice.URL_Hotel
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

/** Rest API
 *
 *  GET     /hotel
 */

/** Retrofit rest API **/

interface RetrofitHotelRestAPI {

    @GET(URL_Hotel)
    fun getHotel(): Deferred<HotelResponse>
}