package aleixbdm.com.techchallenge.webservice.flight

import com.google.gson.annotations.SerializedName
import java.util.*

data class FlightListResponse(
    @SerializedName("flights")
    val flightList: List<FlightResponse>
)

data class FlightResponse(
    val airline: String,
    @SerializedName("departure_date")
    val departureDate: Date,
    @SerializedName("arrival_date")
    val arrivalDate: Date,
    val price: Long,
    @SerializedName("departure_airport")
    val departureAirport: String,
    @SerializedName("arrival_airport")
    val arrivalAirport: String
)