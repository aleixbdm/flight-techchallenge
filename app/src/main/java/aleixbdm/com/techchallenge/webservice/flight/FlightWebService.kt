package aleixbdm.com.techchallenge.webservice.flight

import aleixbdm.com.techchallenge.core.Event
import aleixbdm.com.techchallenge.webservice.resolve
import retrofit2.Retrofit

/** Web service **/

interface FlightWebService {
    suspend fun getFlightList(): Event<FlightListResponse>
}

/** Retrofit **/

private class RetrofitFlightWebService(retrofit: Retrofit) : FlightWebService {

    private val api = retrofit.create(RetrofitFlightRestAPI::class.java)

    override suspend fun getFlightList() = api.getFlightList().resolve()
}

/** Module **/

interface RetrofitFlightWebServiceModule {
    fun webService(retrofit: Retrofit): FlightWebService =
        RetrofitFlightWebService(retrofit)
}