package aleixbdm.com.techchallenge.webservice

import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object WebServiceInjector {
    fun converter(): Gson =
        Gson() // Is the same for -> GsonBuilder().setDateFormat(DateFormat.FULL).create()

    fun retrofit(baseUrl: HttpUrl): Retrofit {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .connectTimeout(2, TimeUnit.SECONDS)
            .readTimeout(2, TimeUnit.SECONDS)
            .writeTimeout(2, TimeUnit.SECONDS)
            .addInterceptor(logging)
            .build()
        val converter = converter()
        return Retrofit.Builder()
            .client(client)
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(converter))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }

    class Module(private val retrofit: Retrofit) {
        fun retrofit(): Retrofit = retrofit
    }
}