package aleixbdm.com.techchallenge.webservice.hotel

import com.google.gson.annotations.SerializedName

data class HotelResponse(
    val name: String,
    @SerializedName("hotel_location")
    val location: String,
    val description: String,
    val images: List<String>,
    val rating: Int,
    val facilities: List<String>
)