package aleixbdm.com.techchallenge.webservice.hotel

import aleixbdm.com.techchallenge.core.Event
import aleixbdm.com.techchallenge.webservice.resolve
import retrofit2.Retrofit

/** Web service **/

interface HotelWebService {
    suspend fun getHotel(): Event<HotelResponse>
}

/** Retrofit **/

private class RetrofitHotelWebService(retrofit: Retrofit) : HotelWebService {

    private val api = retrofit.create(RetrofitHotelRestAPI::class.java)

    override suspend fun getHotel() = api.getHotel().resolve()
}

/** Module **/

interface RetrofitHotelWebServiceModule {
    fun webService(retrofit: Retrofit): HotelWebService =
        RetrofitHotelWebService(retrofit)
}