package aleixbdm.com.techchallenge.webservice

import aleixbdm.com.techchallenge.core.Event
import kotlinx.coroutines.Deferred

suspend fun <T> Deferred<T>.resolve(): Event<T> {
    return try {
        Event.success(await())
    } catch (error: Throwable) {
        Event.error(WebServiceError(error))
    }
}