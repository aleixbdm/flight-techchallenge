package aleixbdm.com.techchallenge.webservice.flight

import aleixbdm.com.techchallenge.webservice.URL_FlightList
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

/** Rest API
 *
 *  GET     /flightList
 */

/** Retrofit rest API **/

interface RetrofitFlightRestAPI {

    @GET(URL_FlightList)
    fun getFlightList(): Deferred<FlightListResponse>
}