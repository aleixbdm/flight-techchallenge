package aleixbdm.com.techchallenge.viewmodel.hotel

import aleixbdm.com.techchallenge.core.Event
import aleixbdm.com.techchallenge.repository.hotel.HotelRepository
import aleixbdm.com.techchallenge.repository.kotlinTransformationsMap
import aleixbdm.com.techchallenge.viewmodel.ViewModelModule
import aleixbdm.com.techchallenge.viewmodel.ViewModelStatus
import aleixbdm.com.techchallenge.viewmodel.runCoroutine
import androidx.lifecycle.*

/** View Model **/

abstract class HotelViewModel : ViewModel(), LifecycleObserver {
    abstract val getHotelStatus: LiveData<Event<ViewModelStatus>>
    abstract val hotelLiveData: LiveData<HotelViewData?>
    abstract fun getHotel()
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        getHotel()
    }
}

/** Definition **/

private class HotelViewModelDefinition constructor(
    private val repository: HotelRepository
) : HotelViewModel() {
    private val _getHotelStatus = MutableLiveData<Event<ViewModelStatus>>()
    override val getHotelStatus: LiveData<Event<ViewModelStatus>>
        get() = _getHotelStatus

    override val hotelLiveData by lazy(LazyThreadSafetyMode.NONE) {
        kotlinTransformationsMap(repository.hotelLiveData) { it?.toViewData() }
    }

    override fun getHotel() {
        _getHotelStatus.value = Event.success(ViewModelStatus.Loading)
        val onCompleted = {
            _getHotelStatus.value = Event.success(ViewModelStatus.Completed)
        }
        val onError = { exception: Exception ->
            _getHotelStatus.value = Event.error(exception)
        }
        runCoroutine(
            coroutine = { repository.getHotel() },
            onCompleted = onCompleted,
            onError = onError
        )
    }
}

/** Module **/

object HotelViewModelModule : ViewModelModule<HotelViewModel, HotelRepository>(
    viewModelResolution = {
        HotelViewModelDefinition(it)
    }
)