package aleixbdm.com.techchallenge.viewmodel.flight

import aleixbdm.com.techchallenge.R
import aleixbdm.com.techchallenge.repository.flight.AirLine
import aleixbdm.com.techchallenge.repository.flight.FlightModel
import androidx.annotation.DrawableRes
import java.text.DecimalFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.time.temporal.ChronoUnit

/** View Data **/

data class FlightViewData(
    val airlineName: String,
    @DrawableRes val airLineImageResource: Int,
    val departureDate: String,
    val departureTime: String,
    val arrivalTime: String,
    val duration: String,
    val price: Long,
    val departureAirport: String,
    val arrivalAirport: String
)

/** Conversion **/

private const val DURATION_PATTERN = "#.00h"
private const val TIME_PATTERN = "HH:mm"

fun FlightModel.toViewData() = {
    val dateFormat = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
    val timeFormat = DateTimeFormatter.ofPattern(TIME_PATTERN)
    val departureInstant = departureDate.toInstant()
    val arrivalInstant = arrivalDate.toInstant()
    val departureLocalDate = LocalDateTime.ofInstant(departureInstant, departureAirport.zoneId())
    val arrivalLocalDate = LocalDateTime.ofInstant(arrivalInstant, arrivalAirport.zoneId())
    val minutes = ChronoUnit.MINUTES.between(departureInstant, arrivalInstant)
    val hours = minutes.toDouble() / 60
    val duration = DecimalFormat(DURATION_PATTERN).format(hours).replace('.', ':')
    FlightViewData(
        airlineName = airline.name,
        airLineImageResource = airline.toImageResource(),
        departureDate = departureLocalDate.format(dateFormat),
        departureTime = departureLocalDate.format(timeFormat),
        arrivalTime = arrivalLocalDate.format(timeFormat),
        duration = duration,
        price = price,
        departureAirport = departureAirport.name,
        arrivalAirport = arrivalAirport.name
    )
}()

private fun AirLine.toImageResource() =
    when (this) {
        AirLine.FastJet -> R.drawable.ic_fast_jet
        AirLine.QuickJet -> R.drawable.ic_quik_jet
        AirLine.SlowJet -> R.drawable.ic_placeholder_airline
    }