package aleixbdm.com.techchallenge.viewmodel

import aleixbdm.com.techchallenge.core.Event
import aleixbdm.com.techchallenge.repository.RepositoryStatus
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

fun ViewModel.runCoroutine(
    coroutine: suspend () -> Event<RepositoryStatus>,
    onCompleted: () -> Unit,
    onError: (Exception) -> Unit
) = viewModelScope.launch {
    val event = coroutine.invoke()
    evaluateEvent(event, onCompleted, onError)
}

private fun evaluateEvent(
    event: Event<RepositoryStatus>,
    onCompleted: () -> Unit,
    onError: (Exception) -> Unit
) =
    event.evaluate(onSuccess = {
        when (it) {
            RepositoryStatus.Completed -> onCompleted()
        }
    }, onError = onError)