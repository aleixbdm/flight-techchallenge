package aleixbdm.com.techchallenge.viewmodel.hotel

import aleixbdm.com.techchallenge.repository.hotel.HotelModel

/** View Data **/

typealias HotelViewData = HotelModel

/** Conversion **/

fun HotelModel.toViewData() = HotelViewData(
    name = name,
    location = location,
    description = description,
    images = images,
    rating = rating,
    facilities = facilities
)