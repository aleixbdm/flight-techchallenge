package aleixbdm.com.techchallenge.viewmodel.flight

import aleixbdm.com.techchallenge.core.Event
import aleixbdm.com.techchallenge.repository.flight.FlightRepository
import aleixbdm.com.techchallenge.repository.kotlinTransformationsMapList
import aleixbdm.com.techchallenge.viewmodel.ViewModelModule
import aleixbdm.com.techchallenge.viewmodel.ViewModelStatus
import aleixbdm.com.techchallenge.viewmodel.runCoroutine
import androidx.lifecycle.*

/** View Model **/

abstract class FlightViewModel : ViewModel(), LifecycleObserver {
    abstract val getFlightListStatus: LiveData<Event<ViewModelStatus>>
    abstract val flightListLiveData: LiveData<List<FlightViewData>>
    abstract fun getFlightList()
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        getFlightList()
    }
}

/** Definition **/

private class FlightViewModelDefinition constructor(
    private val repository: FlightRepository
) : FlightViewModel() {
    private val _getFlightListStatus = MutableLiveData<Event<ViewModelStatus>>()
    override val getFlightListStatus: LiveData<Event<ViewModelStatus>>
        get() = _getFlightListStatus

    override val flightListLiveData by lazy(LazyThreadSafetyMode.NONE) {
        kotlinTransformationsMapList(repository.flightListLiveData) { list ->
            list.map { it.toViewData() }
        }
    }

    override fun getFlightList() {
        _getFlightListStatus.value = Event.success(ViewModelStatus.Loading)
        val onCompleted = {
            _getFlightListStatus.value = Event.success(ViewModelStatus.Completed)
        }
        val onError = { exception: Exception ->
            _getFlightListStatus.value = Event.error(exception)
        }
        runCoroutine(
            coroutine = { repository.getFlightList() },
            onCompleted = onCompleted,
            onError = onError
        )
    }
}

/** Module **/

object FlightViewModelModule : ViewModelModule<FlightViewModel, FlightRepository>(
    viewModelResolution = {
        FlightViewModelDefinition(it)
    }
)