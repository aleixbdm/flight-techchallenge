package aleixbdm.com.techchallenge.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

abstract class ViewModelModule<in VM : ViewModel, in Res>(
    private val viewModelResolution: (Res) -> VM
) {

    @Suppress("UNCHECKED_CAST")
    fun factory(resolver: Res) =
        object : ViewModelProvider.NewInstanceFactory() {
            override fun <V : ViewModel> create(modelClass: Class<V>): V {
                return viewModelResolution(resolver) as V
            }
        }
}