package aleixbdm.com.techchallenge.app

import aleixbdm.com.techchallenge.database.AppDatabase
import aleixbdm.com.techchallenge.database.DatabaseModule
import aleixbdm.com.techchallenge.database.flight.FlightDao
import aleixbdm.com.techchallenge.database.flight.FlightDaoModule
import aleixbdm.com.techchallenge.repository.flight.FlightRepository
import aleixbdm.com.techchallenge.repository.flight.FlightRepositoryModule
import aleixbdm.com.techchallenge.repository.hotel.HotelRepository
import aleixbdm.com.techchallenge.repository.hotel.HotelRepositoryModule
import aleixbdm.com.techchallenge.viewmodel.ManualViewModelResolverModule
import aleixbdm.com.techchallenge.viewmodel.ViewModelResolver
import aleixbdm.com.techchallenge.viewmodel.flight.FlightViewModel
import aleixbdm.com.techchallenge.viewmodel.flight.FlightViewModelModule
import aleixbdm.com.techchallenge.viewmodel.hotel.HotelViewModel
import aleixbdm.com.techchallenge.viewmodel.hotel.HotelViewModelModule
import aleixbdm.com.techchallenge.webservice.flight.FlightWebService
import aleixbdm.com.techchallenge.webservice.flight.RetrofitFlightWebServiceModule
import aleixbdm.com.techchallenge.webservice.hotel.HotelWebService
import aleixbdm.com.techchallenge.webservice.hotel.RetrofitHotelWebServiceModule
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import retrofit2.Retrofit

/** App Component **/

interface ManualAppComponent : AppComponent {
    companion object {
        fun inject(viewModelResolver: ViewModelResolver): AppComponent {
            return object : ManualAppComponent {
                override val viewModelResolver = viewModelResolver
            }
        }

        fun inject(appModule: AppModule) = {
            val applicationContext = appModule.applicationContext()
            val retrofit = appModule.retrofitModule().retrofit()
            val hotelRepository = lazy {
                RepositoryFactory.hotelRepository(
                    WebServiceFactory.hotelWebService(retrofit)
                )
            }
            val flightRepository = lazy {
                RepositoryFactory.flightRepository(
                    WebServiceFactory.flightWebService(retrofit),
                    DaoFactory.flightDao(
                        DatabaseFactory.database(applicationContext)
                    )
                )
            }
            val viewModelResolver = ViewModelFactory.viewModelResolver(
                hotelRepository,
                flightRepository
            )
            inject(viewModelResolver)
        }()
    }

    val viewModelResolver: ViewModelResolver

    override fun inject(fragment: InjectableFragment) {
        fragment.viewModelResolver = viewModelResolver
    }
}

/** Web service **/

private object WebServiceFactory {
    private val hotelModule by lazy { object : RetrofitHotelWebServiceModule {} }
    private val flightModule by lazy { object : RetrofitFlightWebServiceModule {} }
    fun hotelWebService(retrofit: Retrofit) =
        hotelModule.webService(retrofit)

    fun flightWebService(retrofit: Retrofit) =
        flightModule.webService(retrofit)
}

/** Database **/

private object DatabaseFactory {
    private val module by lazy { object : DatabaseModule {} }
    fun database(applicationContext: Context) =
        module.database(applicationContext)
}

/** Dao **/

private object DaoFactory {
    private val flightModule by lazy { object : FlightDaoModule {} }
    fun flightDao(database: AppDatabase) =
        flightModule.dao(database)
}

/** Repository **/

private object RepositoryFactory {
    private val hotelModule by lazy { object : HotelRepositoryModule {} }
    private val flightModule by lazy { object : FlightRepositoryModule {} }
    fun hotelRepository(webService: HotelWebService) =
        hotelModule.repository(webService)

    fun flightRepository(webService: FlightWebService, dao: FlightDao) =
        flightModule.repository(webService, dao)
}

/** ViewModel **/

private object ViewModelFactory {
    fun viewModelResolver(
        hotelRepository: Lazy<HotelRepository>,
        flightRepository: Lazy<FlightRepository>
    ) = {
        val viewModelsMap: Map<Class<out ViewModel>, Lazy<ViewModelProvider.Factory>> = mapOf(
            HotelViewModel::class.java to
                    lazy {
                        val repository by hotelRepository
                        HotelViewModelModule.factory(repository)
                    },
            FlightViewModel::class.java to
                    lazy {
                        val repository by flightRepository
                        FlightViewModelModule.factory(repository)
                    }
        )
        ManualViewModelResolverModule.viewModelResolver(viewModelsMap)
    }()
}