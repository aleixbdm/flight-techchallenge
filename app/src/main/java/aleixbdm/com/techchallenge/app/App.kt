package aleixbdm.com.techchallenge.app

import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import android.app.Application
import android.content.Context
import okhttp3.HttpUrl

/** App **/

open class App : Application() {

    open val component: AppComponent by lazy {
        val module = AppModuleDefinition(applicationContext)
        AppInjector.manualComponent(module)
    }
}

/** Definition **/

private const val SERVER_URL = "https://pastebin.com/raw/"

class AppModuleDefinition(private val applicationContext: Context) : AppModule {
    override fun applicationContext() =
        applicationContext

    override fun retrofitModule() = {
        val localServerUrl = HttpUrl.get(SERVER_URL)
        val retrofit = WebServiceInjector.retrofit(localServerUrl)
        WebServiceInjector.Module(retrofit)
    }()
}