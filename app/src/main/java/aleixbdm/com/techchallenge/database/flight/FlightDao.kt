package aleixbdm.com.techchallenge.database.flight

import aleixbdm.com.techchallenge.database.AppDatabase
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/** Dao **/

@Dao
interface FlightDao {
    @Query("SELECT * FROM flight ORDER BY departureDate ASC, arrivalAirport ASC, price ASC")
    fun getFlightList(): LiveData<List<FlightEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFlightList(flightList: List<FlightEntity>)
}

/** Module **/

interface FlightDaoModule {
    fun dao(database: AppDatabase): FlightDao {
        return database.flightDao()
    }
}