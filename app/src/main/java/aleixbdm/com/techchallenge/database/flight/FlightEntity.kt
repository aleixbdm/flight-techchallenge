package aleixbdm.com.techchallenge.database.flight

import aleixbdm.com.techchallenge.webservice.flight.FlightResponse
import androidx.room.Entity

/** Entity **/

@Entity(tableName = "flight", primaryKeys = ["airline", "departureDate", "price", "arrivalAirport"])
data class FlightEntity(
    val airline: String,
    val departureDate: Long,
    val arrivalDate: Long,
    val price: Long,
    val departureAirport: String,
    val arrivalAirport: String
)

/** Conversion **/

fun FlightResponse.toEntity() = FlightEntity(
    airline = airline,
    departureDate = departureDate.time,
    arrivalDate = arrivalDate.time,
    price = price,
    departureAirport = departureAirport,
    arrivalAirport = arrivalAirport
)