package aleixbdm.com.techchallenge.database

import aleixbdm.com.techchallenge.database.flight.FlightDao
import aleixbdm.com.techchallenge.database.flight.FlightEntity
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/** Database **/

@Database(entities = [FlightEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun flightDao(): FlightDao
}

/** Module **/

private const val DATABASE_NAME = "tech_challenge_db"

interface DatabaseModule {
    fun database(applicationContext: Context): AppDatabase =
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            DATABASE_NAME
        )
            .fallbackToDestructiveMigration()
            .build()
}